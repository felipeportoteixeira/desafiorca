package felipe.extrato;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTitle(R.string.empty_title);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

    }

    public void Login(View v) {

        EditText editText1 = findViewById(R.id.input_label1);
        String label1 = editText1.getText().toString();
        EditText editText2 = findViewById(R.id.input_label2);
        String label2 = editText2.getText().toString();

        if ((label1.equals(label2)) && (label1.equals("") == false)) {
            Intent intent = new Intent(getBaseContext(), ExtratoActivity.class);
            startActivity(intent);
        }else{
            AlertDialog.Builder alert = new AlertDialog.Builder(this);
            alert.setTitle("Erro");
            alert.setMessage("Login inválido");
            alert.setPositiveButton("OK", null);
            alert.setCancelable(true);
            alert.create().show();

        }

    }

}
