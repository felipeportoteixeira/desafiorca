package felipe.extrato;

import android.os.AsyncTask;
import android.widget.TextView;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;


public class getData extends AsyncTask<Void,Void,Void> {
    String result;
    static User user = new User();

    @Override
    protected Void doInBackground(Void... voids) {

        try {
            URL url = new URL("http://www.icoded.com.br/api/extract.php?pwd=123456");
            HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
            InputStream inputStream = httpURLConnection.getInputStream();
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
            String line = "";

            while(line != null){
                line = bufferedReader.readLine();
                result = result + line;
            }
            user = Conversion.convert(result);

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);

        ExtratoActivity.user.setText(user.getName());
        ExtratoActivity.available.setText(user.getAvailable());
        ExtratoActivity.limit.setText(user.getTotal());
        ExtratoActivity.expent.setText(user.getExpent());

        ExtratoActivity.installment1.setText(user.getInstallments().get(0).getPast_due() +"  |  "+ user.getInstallments().get(0).getCarnet() +
                "  |  "+  user.getInstallments().get(0).getInstallment() +"  |  "+ user.getInstallments().get(0).getValue());
        ExtratoActivity.installment2.setText(user.getInstallments().get(1).getPast_due() +"  |  "+ user.getInstallments().get(1).getCarnet() +
                "  |  "+  user.getInstallments().get(1).getInstallment() +"  |  "+ user.getInstallments().get(1).getValue());
        ExtratoActivity.installment3.setText(user.getInstallments().get(2).getPast_due() +"  |  "+ user.getInstallments().get(2).getCarnet() +
                "  |  "+  user.getInstallments().get(2).getInstallment() +"  |  "+ user.getInstallments().get(2).getValue());
        ExtratoActivity.installment4.setText(user.getInstallments().get(3).getPast_due() +"  |  "+ user.getInstallments().get(3).getCarnet() +
                "  |  "+  user.getInstallments().get(3).getInstallment() +"  |  "+ user.getInstallments().get(3).getValue());
        ExtratoActivity.installment5.setText(user.getInstallments().get(4).getPast_due() +"  |  "+ user.getInstallments().get(4).getCarnet() +
                "  |  "+  user.getInstallments().get(4).getInstallment() +"  |  "+ user.getInstallments().get(4).getValue());
        ExtratoActivity.installment6.setText(user.getInstallments().get(5).getPast_due() +"  |  "+ user.getInstallments().get(5).getCarnet() +
                "  |  "+  user.getInstallments().get(5).getInstallment() +"  |  "+ user.getInstallments().get(5).getValue());
        ExtratoActivity.installment7.setText(user.getInstallments().get(6).getPast_due() +"  |  "+ user.getInstallments().get(6).getCarnet() +
                "  |  "+  user.getInstallments().get(6).getInstallment() +"  |  "+ user.getInstallments().get(6).getValue());
        ExtratoActivity.installment8.setText(user.getInstallments().get(7).getPast_due() +"  |  "+ user.getInstallments().get(7).getCarnet() +
                "  |  "+  user.getInstallments().get(7).getInstallment() +"  |  "+ user.getInstallments().get(7).getValue());
        ExtratoActivity.installment9.setText(user.getInstallments().get(8).getPast_due() +"  |  "+ user.getInstallments().get(8).getCarnet() +
                "  |  "+  user.getInstallments().get(8).getInstallment() +"  |  "+ user.getInstallments().get(8).getValue());
        ExtratoActivity.installment10.setText(user.getInstallments().get(9).getPast_due() +"  |  "+ user.getInstallments().get(9).getCarnet() +
                "  |  "+  user.getInstallments().get(9).getInstallment() +"  |  "+ user.getInstallments().get(9).getValue());
        ExtratoActivity.installment11.setText(user.getInstallments().get(10).getPast_due() +"  |  "+ user.getInstallments().get(10).getCarnet() +
                "  |  "+  user.getInstallments().get(10).getInstallment() +"  |  "+ user.getInstallments().get(10).getValue());

    }

    public static User getUser() {
        return user;
    }
}
