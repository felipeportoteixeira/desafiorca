package felipe.extrato;

import java.util.ArrayList;

public class Conversion {

    public static User convert(String r){

        User u = new User();

        int indexName = r.indexOf("name") +7;
        String userName = "";
        while (r.charAt(indexName) != '\"'){
            userName += r.charAt(indexName);
            indexName++;
        }

        int indexStatus = r.indexOf("status") +9;
        String userStatus = "";
        while (r.charAt(indexStatus) != '\"'){
            userStatus += r.charAt(indexStatus);
            indexStatus++;
        }

        int indexCode = r.indexOf("code") + 6;
        String userCode = "";
        while (r.charAt(indexCode) != ','){
            userCode += r.charAt(indexCode);
            indexCode++;
        }

        int indexTotal_overdue = r.indexOf("total_overdue") +16;
        String userTotal_overdue = "";
        while (r.charAt(indexTotal_overdue) != '\"'){
            userTotal_overdue += r.charAt(indexTotal_overdue);
            indexTotal_overdue++;
        }

        int indexTotal_due = r.indexOf("total_due") +12;
        String userTotal_due = "";
        while (r.charAt(indexTotal_due) != '\"'){
            userTotal_due += r.charAt(indexTotal_due);
            indexTotal_due++;
        }

        int indexTotal = r.indexOf("\"total\"") +9;
        String userTotal = "";
        while (r.charAt(indexTotal) != '\"'){
            userTotal += r.charAt(indexTotal);
            indexTotal++;
        }

        int indexExpent = r.indexOf("\"expent\"") +10;
        String userExpent = "";
        while (r.charAt(indexExpent) != '\"'){
            userExpent += r.charAt(indexExpent);
            indexExpent++;
        }

        int indexAvailable = r.indexOf("\"available\"") +13;
        String userAvailable = "";
        while (r.charAt(indexAvailable) != '\"'){
            userAvailable += r.charAt(indexAvailable);
            indexAvailable++;
        }

        ArrayList<Installment> userInstallments = new ArrayList();
        Installment i = new Installment();

        int start = r.indexOf(":[");
        int end = r.indexOf("],");
        int test = 1;

        while(start < end){
            if(r.charAt(start) == '\"' && r.charAt(start-1) == ':'){
                start++;
                String aux = "";
                while (r.charAt(start) != '\"'){
                    aux += r.charAt(start);
                    start++;

                }
                switch (test){
                    case 1:
                        i = new Installment();
                        i.setPast_due(aux);
                        test++;
                        break;
                    case 2:
                        i.setCarnet(aux);
                        test++;
                        break;
                    case 3:
                        i.setInstallment(aux);
                        test++;
                        break;
                    case 4:
                        i.setValue(aux);
                        test++;
                        break;
                    case 5:
                        i.setOverdue_days(aux);
                        test++;
                        break;
                    case 6:
                        i.setOverdue_date(aux);
                        test++;
                        break;
                    case 7:
                        i.setOriginal_value(aux);
                        test++;
                        break;
                    case 8:
                        i.setValue_diff(aux);
                        test++;
                        break;
                    case 9:
                        i.setTotal_value(aux);
                        test++;
                        break;
                    case 10:
                        i.setStore(aux);
                        test = 1;
                        userInstallments.add(i);
                        i = null;
                        break;
                }

            }else{
                start++;
            }

        }


        u.setCode(Integer.parseInt(userCode));
        u.setName(userName);
        u.setStatus(userStatus);
        u.setTotal_overdue(userTotal_overdue);
        u.setTotal_due(userTotal_due);
        u.setTotal(userTotal);
        u.setExpent(userExpent);
        u.setAvailable(userAvailable);
        u.setInstallments(userInstallments);

        return u;
    }

}
