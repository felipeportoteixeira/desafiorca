package felipe.extrato;

import java.util.ArrayList;

public class User {
    private int code;
    private String status;
    private String name;
    private String total_overdue;
    private String total_due;
    private String total;
    private String expent;
    private String available;
    private ArrayList<Installment> installments = new ArrayList();


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getTotal_due() {
        return total_due;
    }

    public void setTotal_due(String total_due) {
        this.total_due = total_due;
    }

    public String getTotal_overdue() {
        return total_overdue;
    }

    public void setTotal_overdue(String total_overdue) {
        this.total_overdue = total_overdue;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getExpent() {
        return expent;
    }

    public void setExpent(String expent) {
        this.expent = expent;
    }

    public String getAvailable() {
        return available;
    }

    public void setAvailable(String available) {
        this.available = available;
    }

    public ArrayList<Installment> getInstallments() {
        return installments;
    }

    public void setInstallments(ArrayList<Installment> installments) {
        this.installments = installments;
    }

}
