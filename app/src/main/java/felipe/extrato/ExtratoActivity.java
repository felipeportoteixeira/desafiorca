package felipe.extrato;

import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.w3c.dom.Text;

public class ExtratoActivity extends AppCompatActivity implements View.OnClickListener{

    public static TextView user;
    public static TextView available;
    public static TextView limit;
    public static TextView expent;
    public static TextView installment1;
    public static TextView installment2;
    public static TextView installment3;
    public static TextView installment4;
    public static TextView installment5;
    public static TextView installment6;
    public static TextView installment7;
    public static TextView installment8;
    public static TextView installment9;
    public static TextView installment10;
    public static TextView installment11;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_extrato);

        Button b1 = (Button) findViewById(R.id.bt1);
        b1.setOnClickListener(this);
        Button b2 = (Button) findViewById(R.id.bt2);
        b2.setOnClickListener(this);
        Button b3 = (Button) findViewById(R.id.bt3);
        b3.setOnClickListener(this);
        Button b4 = (Button) findViewById(R.id.bt4);
        b4.setOnClickListener(this);
        Button b5 = (Button) findViewById(R.id.bt5);
        b5.setOnClickListener(this);
        Button b6 = (Button) findViewById(R.id.bt6);
        b6.setOnClickListener(this);
        Button b7 = (Button) findViewById(R.id.bt7);
        b7.setOnClickListener(this);
        Button b8 = (Button) findViewById(R.id.bt8);
        b8.setOnClickListener(this);
        Button b9 = (Button) findViewById(R.id.bt9);
        b9.setOnClickListener(this);
        Button b10 = (Button) findViewById(R.id.bt10);
        b10.setOnClickListener(this);
        Button b11 = (Button) findViewById(R.id.bt11);
        b11.setOnClickListener(this);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        user = findViewById(R.id.user);
        available = findViewById(R.id.available);
        limit = findViewById(R.id.limit);
        expent = findViewById(R.id.expent);
        installment1 = findViewById(R.id.installment1);
        installment2 = findViewById(R.id.installment2);
        installment3 = findViewById(R.id.installment3);
        installment4 = findViewById(R.id.installment4);
        installment5 = findViewById(R.id.installment5);
        installment6 = findViewById(R.id.installment6);
        installment7 = findViewById(R.id.installment7);
        installment8 = findViewById(R.id.installment8);
        installment9 = findViewById(R.id.installment9);
        installment10 = findViewById(R.id.installment10);
        installment11 = findViewById(R.id.installment11);

        getData process = new getData();
        process.execute();

    }

    public void onClick(View v) {

        TextView v1 = findViewById(R.id.installment1);
        TextView v2 = findViewById(R.id.installment2);
        TextView v3 = findViewById(R.id.installment3);
        TextView v4 = findViewById(R.id.installment4);
        TextView v5 = findViewById(R.id.installment5);
        TextView v6 = findViewById(R.id.installment6);
        TextView v7 = findViewById(R.id.installment7);
        TextView v8 = findViewById(R.id.installment8);
        TextView v9 = findViewById(R.id.installment9);
        TextView v10 = findViewById(R.id.installment10);
        TextView v11 = findViewById(R.id.installment11);

        User user = getData.getUser();
        switch (v.getId()) {

            case R.id.bt1:
                v1.setBackgroundColor(Color.parseColor("#f7b022"));
                AlertDialog.Builder alert = new AlertDialog.Builder(this);
                alert.setTitle("Parcela");
                alert.setMessage("Data: "+user.getInstallments().get(0).getOverdue_date()+ "\nValor total: "+
                        user.getInstallments().get(0).getTotal_value());
                alert.setPositiveButton("OK", null);
                alert.setCancelable(true);
                alert.create().show();
                break;
            case R.id.bt2:
                v2.setBackgroundColor(Color.parseColor("#f7b022"));
                AlertDialog.Builder alert2 = new AlertDialog.Builder(this);
                alert2.setTitle("Parcela");
                alert2.setMessage("Data: "+user.getInstallments().get(1).getOverdue_date()+ "\nValor total: "+
                        user.getInstallments().get(1).getTotal_value());
                alert2.setPositiveButton("OK", null);
                alert2.setCancelable(true);
                alert2.create().show();
                break;
            case R.id.bt3:
                v3.setBackgroundColor(Color.parseColor("#f7b022"));
                AlertDialog.Builder alert3 = new AlertDialog.Builder(this);
                alert3.setTitle("Parcela");
                alert3.setMessage("Data: "+user.getInstallments().get(2).getOverdue_date()+ "\nValor total: "+
                        user.getInstallments().get(2).getTotal_value());
                alert3.setPositiveButton("OK", null);
                alert3.setCancelable(true);
                alert3.create().show();
                break;
            case R.id.bt4:
                v4.setBackgroundColor(Color.parseColor("#f7b022"));
                AlertDialog.Builder alert4 = new AlertDialog.Builder(this);
                alert4.setTitle("Parcela");
                alert4.setMessage("Data: "+user.getInstallments().get(3).getOverdue_date()+ "\nValor total: "+
                        user.getInstallments().get(3).getTotal_value());
                alert4.setPositiveButton("OK", null);
                alert4.setCancelable(true);
                alert4.create().show();
                break;
            case R.id.bt5:
                v5.setBackgroundColor(Color.parseColor("#f7b022"));
                AlertDialog.Builder alert5 = new AlertDialog.Builder(this);
                alert5.setTitle("Parcela");
                alert5.setMessage("Data: "+user.getInstallments().get(4).getOverdue_date()+ "\nValor total: "+
                        user.getInstallments().get(4).getTotal_value());
                alert5.setPositiveButton("OK", null);
                alert5.setCancelable(true);
                alert5.create().show();
                break;
            case R.id.bt6:
                v6.setBackgroundColor(Color.parseColor("#f7b022"));
                AlertDialog.Builder alert6 = new AlertDialog.Builder(this);
                alert6.setTitle("Parcela");
                alert6.setMessage("Data: "+user.getInstallments().get(5).getOverdue_date()+ "\nValor total: "+
                        user.getInstallments().get(5).getTotal_value());
                alert6.setPositiveButton("OK", null);
                alert6.setCancelable(true);
                alert6.create().show();
                break;
            case R.id.bt7:
                v7.setBackgroundColor(Color.parseColor("#f7b022"));
                AlertDialog.Builder alert7 = new AlertDialog.Builder(this);
                alert7.setTitle("Parcela");
                alert7.setMessage("Data: "+user.getInstallments().get(6).getOverdue_date()+ "\nValor total: "+
                        user.getInstallments().get(6).getTotal_value());
                alert7.setPositiveButton("OK", null);
                alert7.setCancelable(true);
                alert7.create().show();
                break;
            case R.id.bt8:
                v8.setBackgroundColor(Color.parseColor("#f7b022"));
                AlertDialog.Builder alert8 = new AlertDialog.Builder(this);
                alert8.setTitle("Parcela");
                alert8.setMessage("Data: "+user.getInstallments().get(7).getOverdue_date()+ "\nValor total: "+
                        user.getInstallments().get(7).getTotal_value());
                alert8.setPositiveButton("OK", null);
                alert8.setCancelable(true);
                alert8.create().show();
                break;
            case R.id.bt9:
                v9.setBackgroundColor(Color.parseColor("#f7b022"));
                AlertDialog.Builder alert9 = new AlertDialog.Builder(this);
                alert9.setTitle("Parcela");
                alert9.setMessage("Data: "+user.getInstallments().get(8).getOverdue_date()+ "\nValor total: "+
                        user.getInstallments().get(8).getTotal_value());
                alert9.setPositiveButton("OK", null);
                alert9.setCancelable(true);
                alert9.create().show();
                break;
            case R.id.bt10:
                v10.setBackgroundColor(Color.parseColor("#f7b022"));
                AlertDialog.Builder alert10 = new AlertDialog.Builder(this);
                alert10.setTitle("Parcela");
                alert10.setMessage("Data: "+user.getInstallments().get(9).getOverdue_date()+ "\nValor total: "+
                        user.getInstallments().get(9).getTotal_value());
                alert10.setPositiveButton("OK", null);
                alert10.setCancelable(true);
                alert10.create().show();
                break;
            case R.id.bt11:
                v11.setBackgroundColor(Color.parseColor("#f7b022"));
                AlertDialog.Builder alert11 = new AlertDialog.Builder(this);
                alert11.setTitle("Parcela");
                alert11.setMessage("Data: "+user.getInstallments().get(10).getOverdue_date()+ "\nValor total: "+
                        user.getInstallments().get(10).getTotal_value());
                alert11.setPositiveButton("OK", null);
                alert11.setCancelable(true);
                alert11.create().show();
                break;

        }

    }

}


