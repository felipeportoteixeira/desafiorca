package felipe.extrato;

public class Installment {

    private String past_due;
    private String carnet;
    private String installment;
    private String value;
    private String overdue_days;
    private String overdue_date;
    private String original_value;
    private String value_diff;
    private String total_value;
    private String store;

    public String getPast_due() {
        return past_due;
    }

    public void setPast_due(String past_due) {
        this.past_due = past_due;
    }

    public String getCarnet() {
        return carnet;
    }

    public void setCarnet(String carnet) {
        this.carnet = carnet;
    }

    public String getInstallment() {
        return installment;
    }

    public void setInstallment(String installment) {
        this.installment = installment;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getOverdue_days() {
        return overdue_days;
    }

    public void setOverdue_days(String overdue_days) {
        this.overdue_days = overdue_days;
    }

    public String getOverdue_date() {
        return overdue_date;
    }

    public void setOverdue_date(String overdue_date) {
        this.overdue_date = overdue_date;
    }

    public String getOriginal_value() {
        return original_value;
    }

    public void setOriginal_value(String original_value) {
        this.original_value = original_value;
    }

    public String getValue_diff() {
        return value_diff;
    }

    public void setValue_diff(String value_diff) {
        this.value_diff = value_diff;
    }

    public String getTotal_value() {
        return total_value;
    }

    public void setTotal_value(String total_value) {
        this.total_value = total_value;
    }

    public String getStore() {
        return store;
    }

    public void setStore(String store) {
        this.store = store;
    }

}
